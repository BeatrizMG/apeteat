def contarPali(pali): #contar los caracteres
    
    resultado = dict() #creamos el diccionario                     
    
    for caracter in pali:
        

#Mi intención es que cuente un carácter como palíndromo, y cuando encuentre una repetición de ese carácter
#lo cuente como un carácter independiente, que sería otro palíndromo y lo cuente como una repetición,
#siendo el resutlado un tercer palíndromo.
        if caracter in resultado and resultado[caracter] == resultado [caracter]:
            
            resultado[caracter] += 1
            
            if resultado[caracter] >=1:
                
                resultado[caracter] += 1
            else:
                resultado[caracter] = 1
        else:
            resultado[caracter] = 1
            
#Sumamos los valores de las claves del diccionario
            
    sumaValores = sum(resultado.values()) 
       
    print ('Total de palindromos: ',sumaValores)
    
       
#Función para detectar si es palíndromo.
    
def es_palindromo(pali):
    
    
    p = pali.replace(' ','').lower() #eliminamos espacios y convertimos a minúsculas
    
    if p == p[::-1]:
        print('La palabra {}, es un palíndromo'.format(pali)) 
    else:

        print('La palabra {}, no es un palíndromo'.format(pali)) 
    
  

pali = input('Introduce una palabra: ')
contarPali(pali)
es_palindromo(pali)